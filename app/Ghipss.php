<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Response;
use SoapClient;
use Log;
use Tblresponse;
use Tblrequest;
use Tblerror;

date_default_timezone_set('UTC');
class Ghipss extends Model
{ 
       protected $certificate;
       protected $password;
       protected $sessionid;
       protected $datetime;
       protected $trackingnumber;
       protected $response;

        //protected $certificate = public_path('sec\developers@ghipss.key.pem');
        //protected $password = 'pass1234';
        //protected $Originbank = '300305';
        //protected $location = 'https://ghipss.fin.ghipss.com/SwitchGIP/WSGIP';
        //protected $sessionid = rand(50,99).time(); //generate random numbers from 50-99 and concatenate with time to get 12 unique digits
        //protected $datetime = date('ymdhis');
        //protected $trackingnumber =(strlen(time())>6)?substr(time(), -6):time();
        //protected $response='';
        //protected $error='';
        //protected $ver='';

   public function __construct()
    {
        $this->certificate = public_path('sec\developers@ghipss.key.pem');
        $this->password = 'pass1234';
        $this->sessionid = rand(50,99).time(); //generate random numbers from 50-99 and concatenate with time to get 12 unique digits
        $this->datetime = date('ymdhis');
        $this->trackingnumber = (strlen(time())>6)?substr(time(), -6):time();

        $this->location = 'https://ghipss.fin.ghipss.com/SwitchGIP/WSGIP';
        $this->originbank = '300305';
        
    }  
   
    public function nameEnquiry($accountnumber,$destinationbank,$channelcode,$fcode)
    {       
      $accountname = '';

    try{
          $context = stream_context_create(array(
              'ssl' => array(
                  'verify_peer' => false,
                  'verify_peer_name' => false,
                  'allow_self_signed' => false
              )
          ));


              $options =  array(

               'stream_context' => $context,
               'ssl_method'     => SOAP_SSL_METHOD_TLS,
               'keep_alive'     => true,
               'trace'          => true,
               'cache_wsdl'     => WSDL_CACHE_NONE,
               'local_cert'     => $this->certificate,
               'passphrase'     => $this->password,
               'location'       => $this->location,
               );

              $soapClient = new SoapClient('https://ghipss.fin.ghipss.com/SwitchGIP/WSGIP?wsdl',$options);

   if($fcode=='NEC'){
      $functioncode='230';
       $ver =array(
         'ReqGIPTransaction' => array(
         'Amount'         =>  000000000000,
        'datetime'        =>  $this->datetime,
        'TrackingNum'     =>  $this->trackingnumber,
        'FunctionCode'    =>  $this->functioncode,
        'OrigineBank'     =>  $this->originbank,
        'DestBank'        =>  $destinationbank,
        'SessionID'       =>  $this->sessionid,
        'ChannelCode'     =>  $channelcode,
        'AccountToCredit' =>  $accountnumber,
        'Narration'       =>  'Name Enquiry Credit for '.$accountnumber,
      )
      );
    }elseif ($fcode=='NED'){
      $functioncode='231';
      $ver =array(
        'ReqGIPTransaction' => array(
        'Amount'            =>  000000000000, //$camount,
       'datetime'           =>  $this->datetime,
       'TrackingNum'        =>  $this->trackingnumber,
       'FunctionCode'       =>  $this->functioncode,
       'OrigineBank'        =>  $this->originbank,
       'DestBank'           =>  $destinationbank,
       'SessionID'          =>  $this->sessionid,
       'ChannelCode'        =>  $channelcode,
       'AccountToDebit'     =>  $accountnumber,
       'Narration'          =>  'Name Enquiry Debit for'.$accountnumber,
     )
     );
   }

   $request = new Tblrequest;
   $request->transactiontype  = $fcode;
   $request->trackingnumber   = $this->trackingnumber;
   $request->sessionnumber    = $this->sessionid;
   $request->destinationbank  = $destinationbank;
   $request->channelcode      = $channelcode;
   $request->accountnumber    = $accountnumber;
   //save request to db
   $request->save();

      if($ver==''){
        $errormsg="Please check your functioncode";
        $statuscode='511';

        $response = new Tblerror;
        $response->trackingnumber= $this->trackingnumber;
        $response->errorcode = $statuscode;
        $response->errormsg  = $errormsg;
        $response->save();

        //translate response to an array and returne
      $apiresponse=[
          'status' => $statuscode,
          'errormsg'  =>  $errormsg,         
        ];
      }else{
      $info = $soapClient->GIPTransactionOp($ver);
      $value = get_object_vars($info);
      $statuscode = '200';

      $response = new Tblresponse;
      $response->transactiontype  = $value['FunctionCode'];
      $response->trackingnumber   = $value['TrackingNum'];
      $response->sessionnumber    = $value['SessionID'];
      $response->destinationbank  = $value['DestBank'];
      $response->channelcode      = $value['ChannelCode'];
        if($fcode='NED'){
        $response->accountnumber  = $value['AccountToDebit'];
        $accountname              = $value['NameToDebit'];
        $response->accountname    = $accountname;
        }elseif ($fcode='NEC') {
          $response->accountnumber  = $value['AccountToCredit'];
          $accountname              = $value['NameToCredit'];
          $response->accountname    = $accountname;
        }
      $response->narration        = $value['Narration'];
      $response->acctcode         = $value['AcctCode'];
      $response->approvecode      = $value['AprvCode'];
      //save response to db
      $response->save();


      //translate response to an array and returne
      $apiresponse=[
          'status' => $statuscode,
          'trackingnumber'  =>  $value['TrackingNum'],
          'accountnumber'   =>  $accountnumber,
          'accountname'     =>  $accountname,
          'actioncode'      =>  $value['AcctCode'],
          'approvecode'     =>  $value['AprvCode'],
        ];

    }       

       return $apiresponse;


   } catch (SoapFault $fault ) {
          //errors are captaured in soap faults
          $response = new Tblerror;
          $response->errorcode = $fault->faultcode;
          $response->errormsg =  $fault->faultstring;
          $response->save();

          $soapresponse = ['status'=>'201',
                        'message'=>$fault->faultcode."-".$fault->faultstring
                      ];

          return $soapresponse;

       }

    }



    public function fundTransfer($amount,$accountnumber,$destinationbank,$channelcode,$fcode)
    {
      
        try{
          $context = stream_context_create(array(
              'ssl' => array(
                  'verify_peer' => false,
                  'verify_peer_name' => false,
                  'allow_self_signed' => false
              )
          ));


              $options =  array(
               'stream_context' => $context,
               'ssl_method'     => SOAP_SSL_METHOD_TLS,
               'keep_alive'     => true,
               'trace'          => true,
               'cache_wsdl'     => WSDL_CACHE_NONE,
               'local_cert'     => $this->certificate,
               'passphrase'     => $this->password,
               'location'       => $this->location,
               );

              $soapClient = new SoapClient('https://ghipss.fin.ghipss.com/SwitchGIP/WSGIP?wsdl',$options);

   if($fcode=='FTC'){
      $functioncode='240';
       $ver =array(
         'ReqGIPTransaction'  => array(
         'Amount'             =>  $amount, //$camount,
        'datetime'            =>  $this->datetime,
        'TrackingNum'         =>  $this->trackingnumber,
        'FunctionCode'        =>  $this->functioncode,
        'OrigineBank'         =>  $this->originbank,
        'DestBank'            =>  $destinationbank,
        'SessionID'           =>  $this->sessionid,
        'ChannelCode'         =>  $channelcode,
        'AccountToCredit'     =>  $accountnumber,
        'Narration'           =>  'FTR - Credit '.$accountnumber.' - '.$trackingnumber,
      )
      );
    }elseif ($fcode=='FTD'){
      $functioncode='241';
      $ver =array(
        'ReqGIPTransaction' => array(
        'Amount'            =>  $amount, //$camount,
       'datetime'           =>  $this->datetime,
       'TrackingNum'        =>  $this->trackingnumber,
       'FunctionCode'       =>  $this->functioncode,
       'OrigineBank'        =>  $this->originbank,
       'DestBank'           =>  $destinationbank,
       'SessionID'          =>  $this->sessionid,
       'ChannelCode'        =>  $channelcode,
       'AccountToDebit'     =>  $accountnumber,
       'Narration'          =>  'FTR - Debit '.$accountnumber.' - '.$trackingnumber,
     )
     );
   }

   $request = new Tblrequest;
   $request->transactiontype  = $fcode;
   $request->trackingnumber   = $this->trackingnumber;
   $request->sessionnumber    = $this->sessionid;
   $request->amount           = $amount;
   $request->destinationbank  = $destinationbank;
   $request->channelcode      = $channelcode;
   $request->accountnumber    = $accountnumber;

   $request->save();

      if($ver==''){       

        $response = new Tblerror;
        $response->trackingnumber = $this->trackingnumber;
        $response->errorcode = $statuscode;
        $response->errormsg  = $errormsg;
        $response->save();

        //translate response to an array and returne
      $apiresponse=[
          'status' => '511',
          'errormsg'  =>  'Please check your functioncode',         
        ];
      }else{
      $info = $soapClient->GIPTransactionOp($ver);
      $value = get_object_vars($info);
      $statuscode = '200';

      $response = new Tblresponse;
      $response->transactiontype  = $value['FunctionCode'];
      $response->trackingnumber   = $value['TrackingNum'];
      $response->sessionnumber    = $value['SessionID'];
      $response->destinationbank  = $value['DestBank'];
      $response->channelcode      = $value['ChannelCode'];
        if($fcode='FTD'){
        $response->accountnumber  = $value['AccountToDebit'];
        }elseif ($fcode='FTC') {
          $response->accountnumber  = $value['AccountToCredit'];
        }
      $response->narration        = $value['Narration'];
      $response->acctcode         = $value['AcctCode'];
      $response->approvecode      = $value['AprvCode'];
      //save response to db
      $response->save();


      //translate response to an array and returne
      $apiresponse=[
          'status' => $statuscode,
          'trackingnumber'  =>  $value['TrackingNum'],
          'accountnumber'   =>  $accountnumber,
          'actioncode'      =>  $value['AcctCode'],
          'approvecode'     =>  $value['AprvCode'],
          'narratioin'      =>  $value['Narration'],
        ];
    }
      

       return $apiresponse;


   } catch (SoapFault $fault ) {
            //errors are captaured in soap faults
            $response = new Tblerror;
          $response->errorcode = $fault->faultcode;
          $response->errormsg =  $fault->faultstring;
          $response->save();

          $soapresponse = ['status'=>'201',
                        'message'=>$fault->faultcode."-".$fault->faultstring
                      ];

          return $soapresponse;

         }
    }
}
