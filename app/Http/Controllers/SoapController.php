<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Artisaninweb\SoapWrapper\Facades\SoapWrapper;
class SoapController extends Controller
{
  public function demo()
    {

      $certificate = "{{assets('developers@ghipss.key.pem')}}";
      $passphrase = 'pass1234';
      // Add a new service to the wrapper
      SoapWrapper::add(function ($service) {
          $service
              ->wsdl('https://ghipss.fin.ghipss.com/SwitchGIP/WSGIP?wsdl')                 // The WSDL url
              ->trace(true)            // Optional: (parameter: true/false)
              ->header()               // Optional: (parameters: $namespace,$name,$data,$mustunderstand,$actor)
              ->customHeader()         // Optional: (parameters: $customerHeader) Use this to add a custom SoapHeader or extended class
              ->cookie()               // Optional: (parameters: $name,$value)
              ->location('https://ghipss.fin.ghipss.com/SwitchGIP/WSGIP')             // Optional: (parameter: $location)
              ->certificate($certificate)          // Optional: (parameter: $certLocation)
              ->cache(WSDL_CACHE_NONE) // Optional: Set the WSDL cache
              ->options([
                         'password' => 'pass1234'
                     ]);
              }
            );

      $data = [
          'CurrencyFrom' => 'USD',
          'CurrencyTo'   => 'EUR',
          'RateDate'     => '2014-06-05',
          'Amount'       => '1000'
      ];

      // Using the added service
      SoapWrapper::service('currency', function ($service) use ($data) {
          var_dump($service->getFunctions());
          // var_dump($service->call('GetConversionAmount', [$data])->GetConversionAmountResult);
      });
    }

}
