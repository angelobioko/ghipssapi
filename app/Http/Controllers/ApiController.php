<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request as Requester;
use Request;
use Log;
use App\Ghipss;
use SoapClient;

class ApiController extends Controller
{
    //
    public function nEnquiry()
    {
      $inputs = Request::all();
      //return $inputs;
      $destinationbank = $inputs['destinationbank'];
      $channel = $inputs['channel'];
      $accountnumber = $inputs['accountnumber'];
      $fcode = $inputs['functioncode'];

      $ghipss = new Ghipss();

      $response = $ghipss->nameEnquiry($accountnumber,$destinationbank,$channel,$fcode);

     //$response = Ghipss::nameEnquiry($accountnumber,$destinationbank,$channel,$fcode);

     return $response;
    }

    public function fTransfer()
    {
      $inputs = Request::all();

      $destinationbank = $inputs['destinationbank'];
      $channel = $inputs['channel'];
      $accountnumber = $inputs['accountnumber'];
      $fcode = $inputs['functioncode'];
      $amount = $inputs['amount'];
      $camount = str_pad($amount, 12, '0', STR_PAD_LEFT);  //set amount to 12 digits
      Log::debug($camount);

      $ghipss = new Ghipss();
      $response = $ghipss->fundTransfer($camount,$accountnumber,$destinationbank,$channel,$fcode);
      // $response = Ghipss::fundTransfer($camount,$accountnumber,$destinationbank,$channel,$fcode);

      return $response;
    }
}
