<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblrequests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('transactiontype');
            $table->string('trackingnumber');
            $table->string('sessionnumber');
            $table->string('amount')->nullable();
            $table->string('destinationbank');
            $table->string('channelcode');
            $table->string('accountnumber');
            $table->string('accountname')->nullable();
            $table->string('narration')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblrequest');
    }
}
